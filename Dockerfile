# This Dockerfile uses dbuild for consistent builds.
# https://bitbucket.org/ebsv/docker-build
#
# START PRE-BUILD
#
# # Image repository name
# #
# REPOSITORY="jbulcher/gsutil"
#
# # Image tag
# #
# TAG="0.1.3"
#
# # Perform other pre-build steps here
#
# END PRE-BUILD
#
# ================================
#
# START POST-BUILD
#
# # Perform any post-build steps here
#
# END POST-BUILD

FROM debian:latest

# Install dependencies
RUN DEBIAN_FRONTEND=noninteractive; \
    apt-get update && \
    apt-get -y install \
    wget \
    python \
    # for crcmod installation
    gcc python-dev python-setuptools

RUN easy_install -U pip && \
    pip install -U crcmod

RUN cd /root && \
    wget --no-verbose "https://storage.googleapis.com/pub/gsutil.tar.gz" && \
    tar -xf gsutil.tar.gz

RUN DEBIAN_FRONTEND=noninteractive; \
    apt-get -y purge wget gcc python-dev python-setuptools

ENV PATH "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root/gsutil"