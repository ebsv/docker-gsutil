# jbulcher/gsutil

Debian Linux image with the gsutil binary and crcmod for managing Google Cloud Storage Buckets.

Configure your credentials with:

```
docker run -it --name gsutil_config jbulcher/gsutil gsutil config
```

Commit the config:

```
docker commit gsutil_config my/gsutil
```

Run `gsutil`:

```
docker run --rm my/gsutil gsutil --help
```